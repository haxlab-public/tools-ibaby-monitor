#!/bin/bash
### Script for restarting the device and factory resetting the baby monitor.
echo Powering OFF the device.
python PowerOff.py
sleep 5s
echo Powering ON the device.
python PowerOn.py
sleep 5s 
echo Waiting for initialization period of the device...
sleep 35s
